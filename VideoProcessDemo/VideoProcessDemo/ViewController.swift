//
//  ViewController.swift
//  VideoProcessDemo
//
//  Created by wc on 2018/12/19.
//  Copyright © 2018 One. All rights reserved.
//

import UIKit
import MetalKit
import AVFoundation
import CoreMedia
import MetalPerformanceShaders

class ViewController: UIViewController {
 
    var captureSession = AVCaptureSession.init()
    var sampleBufferCallbackQueue = DispatchQueue(label: "sampleBufferQueue")
    var mtkView = MTKView.init()
    var lastSampleTime: CMTime = CMTime.zero
    
    private let computerFilters: [ShaderProtocol] = [
        Mirror(),
    ]
    
    private let renderFilters: [ShaderProtocol] = [
        Moon()
    ]

    override func viewDidLoad() {
        super.viewDidLoad()
        
        drawUI()
        setupCaptureSession()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        sampleBufferCallbackQueue.sync {
            self.captureSession.startRunning()
        }
    }

    func drawUI() {
        self.mtkView.frame = UIScreen.main.bounds
    
        self.view.addSubview(self.mtkView)
        self.mtkView.device = MetalManager.shared.device
        self.mtkView.framebufferOnly = false
        self.mtkView.delegate = self
        self.mtkView.colorPixelFormat = MetalManager.shared.colorPixelFormat
        self.mtkView.contentMode = .scaleAspectFit
        self.mtkView.isPaused = true
        
        MetalManager.shared.mtkView = self.mtkView
    }
    
    func setupCaptureSession() {
        self.captureSession.beginConfiguration()
        self.captureSession.sessionPreset = AVCaptureSession.Preset.hd1920x1080
        
        guard let camera = AVCaptureDevice.default(for: .video) else {
            print("[error] 无法获取摄像头")
            return
        }
        
        // videoInput
        do {
            let videoInput = try AVCaptureDeviceInput(device: camera)
            if captureSession.canAddInput(videoInput) {
                captureSession.addInput(videoInput)
            }
        } catch {
            print("[error] 无法获取videoInput")
            return
        }
        
        // videoOutPut
        let videoOutput = AVCaptureVideoDataOutput()
        videoOutput.videoSettings = [kCVPixelBufferPixelFormatTypeKey as String:kCVPixelFormatType_32BGRA]
        
        videoOutput.setSampleBufferDelegate(self, queue: sampleBufferCallbackQueue)
        
        if captureSession.canAddOutput(videoOutput) {
            captureSession.addOutput(videoOutput)
        }
        
        // connection
        if let connection = videoOutput.connection(with: .video) {
           connection.videoOrientation = AVCaptureVideoOrientation.portrait
        }
        
        captureSession.commitConfiguration()
    }
}

extension ViewController:AVCaptureVideoDataOutputSampleBufferDelegate {
    func captureOutput(_ output: AVCaptureOutput, didOutput sampleBuffer: CMSampleBuffer, from connection: AVCaptureConnection) {
        guard let pixelBuffer = CMSampleBufferGetImageBuffer(sampleBuffer) else {
            return
        }
        
        let width = CVPixelBufferGetWidthOfPlane(pixelBuffer, 0)
        let height = CVPixelBufferGetHeightOfPlane(pixelBuffer, 0)
        self.mtkView.drawableSize = CGSize(width: width, height: height)
        
        MetalManager.shared.processNext(pixelBuffer)
        lastSampleTime = CMSampleBufferGetPresentationTimeStamp(sampleBuffer)
        
        DispatchQueue.main.sync {
            self.view.layer.contents = pixelBuffer
            mtkView.draw()
        }
    }
}

extension ViewController:MTKViewDelegate {
    func mtkView(_ view: MTKView, drawableSizeWillChange size: CGSize) {
        
    }
    
    func draw(in view: MTKView) {
        guard let currentDrawable = mtkView.currentDrawable,
            let texture = MetalManager.shared.sourceTexture,
            let commandBuffer = MetalManager.shared.commandQueue?.makeCommandBuffer() else {
                return
        }
        // 计算管道
        let shader:ShaderProtocol = computerFilters[0]
        
        shader.encodeComputer(commandBuffer: commandBuffer, sourceTexture: texture, destinationTexture: currentDrawable.texture)
        
        // 渲染管道
        let renderShader: ShaderProtocol = renderFilters[0]
        renderShader.encodeRender(commandBuffer: commandBuffer, sourceTexture: texture, destinationTexture: currentDrawable.texture)
        
        
        commandBuffer.present(currentDrawable)
        commandBuffer.commit()
    }
}


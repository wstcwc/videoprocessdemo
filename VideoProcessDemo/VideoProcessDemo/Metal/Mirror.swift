//
//  Mirror.swift
//  VideoProcessDemo
//
//  Created by mac on 2018/12/20.
//  Copyright © 2018 One. All rights reserved.
//

import Foundation
import Metal

class Mirror:ShaderProtocol {
    var identifier: String
    // 计算管道状态
    var computePipelineState: MTLComputePipelineState?
    // 渲染管道状态
    var renderPipelineState: MTLRenderPipelineState?
    var renderPipelineDescriptor: MTLRenderPipelineDescriptor?
    
    init() {
        self.identifier = "Mirror"
        
        computePipelineState = MetalManager.shared.makeComputePipelineState(functionName: "mirror1")
        
        renderPipelineDescriptor = MetalManager.shared.makeRenderPipelineState(vertexName: nil, fragmentName: nil, colorPixelFormat: MTLPixelFormat.rgba8Unorm)
    }
    // ShaderProtocol
    func encodeComputer(commandBuffer: MTLCommandBuffer, sourceTexture: MTLTexture, destinationTexture: MTLTexture) {
        guard let cps = computePipelineState else {
            return
        }
        
        let computeCommmandEncoder = commandBuffer.makeComputeCommandEncoder()
        
        computeCommmandEncoder?.setComputePipelineState(cps)
        computeCommmandEncoder?.setTexture(sourceTexture, index: 0)
        computeCommmandEncoder?.setTexture(destinationTexture, index: 1)
        // 计算区域
        computeCommmandEncoder?.dispatchThreadgroups(sourceTexture.threadGroups(pipeline: cps),
                                                threadsPerThreadgroup: sourceTexture.threadGroupCount(pipeline:cps))
        
        computeCommmandEncoder?.endEncoding()
        
    }
    func encodeRender(commandBuffer: MTLCommandBuffer, sourceTexture: MTLTexture, destinationTexture: MTLTexture) {
        
    }
}

//
//  Moon.swift
//  VideoProcessDemo
//
//  Created by mac on 2018/12/20.
//  Copyright © 2018 One. All rights reserved.
//

import Foundation
import MetalKit

class Moon: ShaderProtocol {
    var identifier: String
    
    // 渲染管道状态
    var renderPipelineState: MTLRenderPipelineState?
    var renderPipelineDescriptor: MTLRenderPipelineDescriptor?
    
    init() {
        self.identifier = "Moon"
        
        renderPipelineDescriptor = MetalManager.shared.makeRenderPipelineState(vertexName: nil, fragmentName: "moon_main", colorPixelFormat: .rgba8Unorm)
        
        do {
            renderPipelineState = try MetalManager.shared.device.makeRenderPipelineState(descriptor: renderPipelineDescriptor!)
        } catch let error {
            fatalError(error.localizedDescription)
        }
    }
    
    func encodeComputer(commandBuffer: MTLCommandBuffer, sourceTexture: MTLTexture, destinationTexture: MTLTexture) {}
    
    
    func encodeRender(commandBuffer: MTLCommandBuffer, sourceTexture: MTLTexture, destinationTexture: MTLTexture) {
        guard let rps = renderPipelineState,
              let encodeRender = commandBuffer.makeRenderCommandEncoder(descriptor: MetalManager.shared.mtkView!.currentRenderPassDescriptor!)
            else {
            return
        }
        
        encodeRender.setRenderPipelineState(rps)
        
        encodeRender.setVertexBuffer(MetalManager.shared.vertexBuffer,
                                     offset: 0,
                                     index: 0)
        
        encodeRender.setfragment
        
    }
}

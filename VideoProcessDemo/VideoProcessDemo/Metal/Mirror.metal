//
//  Mirror.metal
//  VideoProcessDemo
//
//  Created by mac on 2018/12/20.
//  Copyright © 2018 One. All rights reserved.
//

#include <metal_stdlib>
using namespace metal;

//原始显示
kernel void origintx(texture2d<float, access::read> inTexture [[ texture(0) ]],
                     texture2d<float, access::write> outTexture [[ texture(1) ]],
                     uint2 gid [[ thread_position_in_grid ]]) {
    
    float4 inColor = inTexture.read(gid);
    outTexture.write(inColor, gid);
}

// 左右镜像
kernel void mirror1(texture2d<float, access::read> inTexture [[ texture(0) ]],
                    texture2d<float, access::write> outTexture [[ texture(1) ]],
                    device const float *scale [[ buffer(1) ]],
                    uint2 gid [[ thread_position_in_grid ]])
{
    /****** 左右镜像 *******/
    float4 origin = inTexture.read(gid);
    float4 mir = inTexture.read(uint2(inTexture.get_width() - gid.x, gid.y));
    
    float width = inTexture.get_width();
    float height = inTexture.get_height();
    float2 size = float2(width, height);
    float2 uv = float2(gid) / size;
    
    // 原图(这里减8后才是正常的镜像, 原因暂未找到)
    float2 originSize = float2(width/2 - 2.0, height);
    uint2 oPosition = uint2(uv * originSize); // 相当于cgrect
    origin = inTexture.read(oPosition);  // 读取原图oPosition位置的像素
    outTexture.write(origin, oPosition); // 将读取出来的像素 绘制到指定区域
    
    // 镜像
    float2 mUv = uv;
    mUv.x = mUv.x + 1;
    mUv.y = mUv.y;
    float2 mirrorSize = float2(width/2, height);
    uint2 mPosition = uint2(mUv * mirrorSize);
    // 能获取原图的镜像图片
    mir = inTexture.read(uint2(inTexture.get_width() - mPosition.x, mPosition.y));
    outTexture.write(mir,mPosition);
}

//
//  ShaderProtocol.swift
//  VideoProcessDemo
//
//  Created by wc on 2018/12/20.
//  Copyright © 2018 One. All rights reserved.
//

import Foundation
import CoreMedia
import MetalKit
import MetalPerformanceShaders

protocol ShaderProtocol {
    var identifier: String { get }
    func encodeComputer(commandBuffer: MTLCommandBuffer, sourceTexture: MTLTexture, destinationTexture: MTLTexture)
    func encodeRender(commandBuffer: MTLCommandBuffer, sourceTexture: MTLTexture, destinationTexture: MTLTexture)
}

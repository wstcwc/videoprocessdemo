//
//  Extensions.swift
//  VideoProcessDemo
//
//  Created by mac on 2018/12/20.
//  Copyright © 2018 One. All rights reserved.
//

import Foundation
import UIKit

extension MTLTexture {
    func threadGroupCount(pipeline: MTLComputePipelineState) -> MTLSize {
        return MTLSizeMake(pipeline.threadExecutionWidth,
                           pipeline.maxTotalThreadsPerThreadgroup / pipeline.threadExecutionWidth,
                           1)
    }
    
    func threadGroups(pipeline: MTLComputePipelineState) -> MTLSize {
        let groupCount = threadGroupCount(pipeline: pipeline)
        return MTLSizeMake((self.width + groupCount.width - 1) / groupCount.width, (self.height + groupCount.height - 1) / groupCount.height, 1)
    }
}

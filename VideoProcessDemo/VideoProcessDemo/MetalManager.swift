//
//  MetalManager.swift
//  VideoProcessDemo
//
//  Created by wc on 2018/12/19.
//  Copyright © 2018 One. All rights reserved.
//

import UIKit
import MetalKit
import CoreMedia

class MetalManager: NSObject {
    struct LYVertex {
        var position: float3?
        var texture: float2?
    }
    
    var vertices = [
        LYVertex.init(position: float3(-1, 1, 0), texture: float2(0, 1)), // 左上
        LYVertex.init(position: float3(-1, -1, 0), texture: float2(0, 0)), // 左下
        LYVertex.init(position: float3(1, -1, 0), texture: float2(1, 0)), // 右下
        
        LYVertex.init(position: float3(-1, 1, 0), texture: float2(0, 1)), // 左上
        LYVertex.init(position: float3(1, 1, 0), texture: float2(1, 1)), // 右上
        LYVertex.init(position: float3(1, -1, 0), texture: float2(1, 0)), // 右下
    ]
    
    static let shared: MetalManager = MetalManager()
    
    let device = MTLCreateSystemDefaultDevice()!
    var sourceTexture: MTLTexture?
    var destinationTexture: MTLTexture?
    var colorPixelFormat: MTLPixelFormat = .bgra8Unorm
    var mtkView: MTKView?
    
    private var library: MTLLibrary?
    private(set) var commandQueue: MTLCommandQueue?
    private var textureCache: CVMetalTextureCache?
    var vertexBuffer: MTLBuffer?
    
    private override init() {
        super.init()
        
        library = device.makeDefaultLibrary()
        commandQueue = device.makeCommandQueue()
        CVMetalTextureCacheCreate(kCFAllocatorDefault, nil, device, nil, &textureCache)
        
        self.vertexBuffer = self.device.makeBuffer(bytes: self.vertices,
                                                   length: MemoryLayout<LYVertex>.stride * vertices.count,
                                                   options: [])
        
    }
    
    func makeComputePipelineState(functionName: String) -> MTLComputePipelineState? {
        guard let function = library?.makeFunction(name: functionName) else {
            return nil
        }
        
        return try? device.makeComputePipelineState(function: function)
    }
    
    func makeRenderPipelineState(vertexName: String? = nil,
                                 fragmentName: String? = nil,
                                 colorPixelFormat: MTLPixelFormat = .bgra8Unorm) -> MTLRenderPipelineDescriptor? {
        let pipelineDescriptor = MTLRenderPipelineDescriptor()
        pipelineDescriptor.sampleCount = 1
        pipelineDescriptor.colorAttachments[0].pixelFormat = colorPixelFormat
        pipelineDescriptor.depthAttachmentPixelFormat = .invalid
        if let vn = vertexName {
            pipelineDescriptor.vertexFunction = library?.makeFunction(name: vn)
        }
        if let fn = fragmentName {
            pipelineDescriptor.fragmentFunction = library?.makeFunction(name: fn)
        }
        
        return pipelineDescriptor
    }
    
    func processNext(_ pixelBuffer: CVPixelBuffer) {
        guard let tc = textureCache else {
            return
        }
        
        var cvmTexture: CVMetalTexture?
        let width = CVPixelBufferGetWidth(pixelBuffer)
        let height = CVPixelBufferGetHeight(pixelBuffer)
        CVMetalTextureCacheCreateTextureFromImage(kCFAllocatorDefault,
                                                  tc,
                                                  pixelBuffer,
                                                  nil,
                                                  colorPixelFormat,
                                                  width,
                                                  height,
                                                  0,
                                                  &cvmTexture)
        if let cvmTexture = cvmTexture,
           let texture = CVMetalTextureGetTexture(cvmTexture) {
            sourceTexture = texture
        }
    }
    
    func processNext(_ sampleBuffer: CMSampleBuffer) {
        guard let cv = CMSampleBufferGetImageBuffer(sampleBuffer) else {
            return
        }
        
        processNext(cv)
    }
}
